import { Component, OnInit } from '@angular/core';
import { LoginService } from '../login.service';
import { Route, Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username : string;
  password : string;
  
  isError :boolean = false; //กำหนดเป็น false
  errMsg : string =''; //

  userType=[
    {id:'1',name:'ผู้ดูแลระบบ'},
    {id:'2',name:'เจ้าหน้าที่'}
  ]

  constructor(private loginService:LoginService,private router:Router){

  }

  ngOnInit() {
  }

  async doLogin(){
    try{  //ใช้try catch ครอบเอาไว้เผื่อมัน error จะได้ไม่หยุดทำงาน
      var rs:any=await this.loginService.doLogin(this.username,this.password);
      console.log(rs);
        if(rs.ok){
          //success
          sessionStorage.setItem('token',rs.token);
          this.router.navigateByUrl('/admin');
        }else{
          this.isError=true;
          this.errMsg=rs.error;
        }
      }catch(error){
    }
  }

}
