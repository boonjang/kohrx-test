import { Component, OnInit } from '@angular/core';
import { UsersService } from '../users.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  users=[];
  constructor(private usersService: UsersService) { }

  ngOnInit() {
    //เป็น event ไว้ดึงข้อมูลและ render ข้อมูล
    this.getUsers();
  }

  //ต้องแยก method ออกมาเผื่อมีการถูกเรีกบ่อยๆ
  async getUsers() {
    let token = sessionStorage.getItem('token');
    try {
      var rs: any = await this.usersService.getUsers(token);
      if (rs.ok) {
        this.users = rs.rows;
      } else {
        console.log(rs.error);
      }
    } catch (error) {
      console.log(error);
    }
  }
}