import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  collapsed=true;
  title = 'Mahachanachai Hospital';

  a:number;
  b:number;
  c:number =0;

  fluts: any=['apple','banana','mango'];

  users: any = [
    {
      "id": 1,
      "fullname": "สถิตย์ เรียนพิศ",
      "username": "admin"
    },
    {
      "id": 3,
      "fullname": "Satit Rianpit",
      "username": "xxxx"
    }
  ];
  constructor(){
    this.c=this.a+this.b;
  }
  addNumber(){
    this.c=+this.a+ +this.b; //การใส่เครื่องหมาย + หน้าตัวแปรเพื่อเป็นการแปลงเป็นตัวเลข
  }
}
